import React, {Component} from 'react';
import { Root } from "native-base";
import MainRoute from "./src/components/routes/Route";
export default class App extends Component {
render() {
return (
<Root>
   <MainRoute />
</Root>
);
}
}