import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Header, Content, Button, Text, Left, Body, Right, Icon, Title, Form, Item, Input, Label } from 'native-base';
import MapView, { Permissions } from 'react-native-maps';

export default class CurrentLocation extends Component {
   
    render() {
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Current Location</Title>
                    </Body>
                    <Right>
<Button transparent onPress={() => this.props.navigation.navigate('TrackLocation')}>
<Icon name='add' />
</Button>
</Right>
                </Header>
                    <MapView style={styles.map}
                        initialRegion={{
                            latitude: 17.5259867,
                            longitude: 78.268801,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }} />
            </Container>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        // position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        flex:1
        // alignItems: 'center',
        // justifyContent: 'flex-end',
    },
    map: {
        position: 'absolute',
        height: 800,
        flex: 1,
        justifyContent: 'flex-end',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0

    },
});