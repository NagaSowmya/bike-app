import React, { Component } from 'react';
import { Text, View, StyleSheet,Alert } from 'react-native';
import MapView,{  Location, Permissions } from 'react-native-maps';

export default class CurrentPermissions extends Component {
  state = {
    latitude:0,
    longitude:0
  };

 async componentDidMount() {
    const { status } = await Permissions.getAsync(Permissions.LOCATION);
    if (status !== 'granted') {
            const response = await Permissions.askAsync(Permissions.LOCATION);
}
navigator.geolocation.getCurrentPosition(
    ({coords:{latitude,longitude}})=>this.setState({latitude,longitude},()=>Alert.alert('State:'+this.setState)),
(err)=>Alert.alert('Error'+err)
)
 };

//   _handleMapRegionChange = mapRegion => {
//     this.setState({ mapRegion });
//   };

//   _getLocationAsync = async () => {
//    let { status } = await Permissions.askAsync(Permissions.LOCATION);
//    if (status !== 'granted') {
//      this.setState({
//        locationResult: 'Permission to access location was denied',
//        location,
//      });
//    }

//    let location = await Location.getCurrentPositionAsync({});
//    this.setState({ locationResult: JSON.stringify(location), location, });
//  };

  render() {
      const {latitude,longitude}=this.state;
    return (
      <View style={styles.container}>
        <MapView style={{flex:1}}
                        initialRegion={{
                            latitude,
                            longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }} />
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});
