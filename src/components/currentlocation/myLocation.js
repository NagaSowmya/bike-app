import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Alert,
    TouchableOpacity, Dimensions, Image
} from "react-native";
// import Permissions from 'react-native-permissions'
// import { PermissionsAndroid } from 'react-native';
// import PermissionsList from './Utitliy/PermissionsList'
import MapView, { Polyline, Marker } from 'react-native-maps';
import PolyLine from "@mapbox/polyline";

import apiKey from '../google_api_key/google_api_key';
const locations = require('../locations/locations.json');

const { width, height } = Dimensions.get('window');
const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

export default class myLocation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            latitude: null,
            longitude: null,
            locations: locations,
        }
    }
    async componentDidMount() {
        // const { status } = await Permissions.getAsync(Permissions.LOCATION);
        // if (status !== 'granted') {
        //     const response = await Permissions.askAsync(Permissions.LOCATION);
        // }
        navigator.geolocation.getCurrentPosition(
            ({ coords: { latitude, longitude } }) => this.setState({ latitude, longitude }, this.mergeCoords),
            error => console.error(error),
            { enableHighAccuracy: true, maximumAge: 2000, timeout: 20000 }
        )
        const { locations: [sampleLocation] } = this.state
        this.setState({
            desLatitude: sampleLocation.coords.latitude,
            desLongitude: sampleLocation.coords.longitude

        }, this.mergeCoords)
    }

    mergeCoords = () => {
        const {
            latitude,
            longitude,
            desLatitude,
            desLongitude
        } = this.state

        const hasStartAndEnd = latitude !== null && desLatitude !== null
        if (hasStartAndEnd) {
            const concatStart = `${latitude},${longitude}`
            const concatEnd = `${desLatitude},${desLongitude}`
            this.getDirections(concatStart, concatEnd)

        }
    }

    // componentWillMount() {
    //     this.getDirections();
    // }

    async getDirections(startLoc, desLoc) {
        console.log("source and destination lat long" + startLoc + "&&&" + desLoc);
        try {
            const response = await fetch(
                `https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc}&destination=${desLoc}&key=AIzaSyCpPFNQ3X2AhXy_ekFzj0fxTTiQyVkfecs`
            );
            const resJson = await response.json();
            console.log("data", resJson);
            const points = PolyLine.decode(resJson.routes[0].overview_polyline.points);
            const coords = points.map(point => {
                return {
                    latitude: point[0],
                    longitude: point[1]
                };
            });
            this.setState({ coords });
        } catch (error) {
            console.log(error);
        }
    }
    // onMarkerPress=location=>()=>{
    // console.log("onMarkerPress",location)
    // const{coords:{latitude,longitude}}=location
    // this.setState({
    //     destination:location,
    //     desLatitude:latitude,
    //     desLongitude:longitude
    // },this.mergeCoords())
    // }
    // renderMarkers=()=>{
    // const {locations}=this.state
    // return(
    //     <View>
    //         {
    //             locations.map((location,idx)=>{
    //                 const {
    //                     coords:{latitude,longitude}=location
    //                 }
    //                 return(
    //                     <Marker
    //                     key={idx}
    //                     coordinate={{latitude,longitude}}
    //                     onPress={this.onMarkerPress(location)}
    //                     />
    //                 )
    //             })
    //         }
    //     </View>
    // )
    // }
    render() {
        const { latitude, longitude, coords, destination } = this.state;
        if (latitude) {


            return (
                <MapView style={styles.mapStyle}
                    showsUserLocation={true}
                    region={{
                        latitude: latitude,
                        longitude: longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121
                    }}
                >
                    <Polyline
                        coordinates={coords}
                        strokeWidth={2}
                        strokeColor="red"
                    />
                    
                </MapView>
            );
        }

        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>We need your Permissions</Text>
            </View>
        )
        // else {
        //     return null
        // }
    }
}
const styles = {
    containerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lightblue'
    },

    mapStyle: {
        // flex: 1,
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute'
    }

}