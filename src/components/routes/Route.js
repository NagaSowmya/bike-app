import React from "react";
// import { View, Text, Button } from "react-native";
import LeftDrawer from "./LeftDrawer"
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Home from '../home/Home';
import Login from "../login/Login";
import CurrentLocation from '../currentlocation/CurrentLocation';
import CurrentPermissions from '../currentlocation/CurrentPermissions';
import FindLocation from '../currentlocation/FindLocation';
import myLocation from '../currentlocation/myLocation';
import TrackLocation from '../currentlocation/TrackLocation';
import Geolocation from '../currentlocation/geoLocation';
const MainNavigator = createStackNavigator({
    Home: {screen: Home},
Login: {screen: Login},
CurrentLocation: { screen: CurrentLocation },
CurrentPermissions:{screen:CurrentPermissions},
LeftDrawer: {screen: LeftDrawer},
FindLocation:{screen:FindLocation},
myLocation:{screen:myLocation},
TrackLocation:{screen:TrackLocation},
Geolocation:{screen: Geolocation}
},
{
initialRouteName: "LeftDrawer",
headerMode: "none",
// swipeEnabled: false
});
const MainRoute = createAppContainer(MainNavigator);
export default MainRoute;