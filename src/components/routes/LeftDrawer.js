import React from "react";
import { View, Text, Button, Dimensions } from "react-native";
import { createDrawerNavigator, createAppContainer } from "react-navigation";
import LeftSideBar from "../sidebar/LeftsideBar";
import CurrentLocation from '../currentlocation/CurrentLocation';
import Login from "../login/Login";
import Home from '../home/Home';
import myLocation from '../currentlocation/myLocation';

const WIDTH = Dimensions.get('window').width;
const LeftDrawer = createDrawerNavigator(
{
    CurrentLocation: { screen: CurrentLocation },
    Login: { screen: Login },
    Home: {screen: Home},
    myLocation:{screen:myLocation}

},
{
initialRouteName: "Home",
drawerWidth:WIDTH*0.80,
drawerPosition:'left',
contentOptions: {
activeTintColor: "#e91e63"
},
contentComponent: props => <LeftSideBar {...props} />,
drawerOpenRoute: 'LeftSideMenu',
drawerCloseRoute: 'LeftSideMenuClose',
drawerToggleRoute: 'LeftSideMenuToggle',
}
);
export default LeftDrawer;